# README #

A Dockerfile and a docker-compose.yml file that quickly gets a DJB style dnscache server 
and tinydns server running in docker containers. 

This is intended to deliver a "split-horizon" DNS service.

### Why would you want to do this? ###

* Super-lightweight - You don't need VMs to isolate your DNS servers
* Makes it easy for your DNS servers to be portable and replicable

### Setup ###
Make sure __docker__ and __docker-compose__ are ready to use
```
$ docker -v
Docker version 1.12.3, build 6b644ec
$ docker-compose -v
docker-compose version 1.9.0, build 2585387
```
Clone the repository somewhere.
```
~/docker$ git clone git@bitbucket.org:SeamanJeff/jadhcp.git
Cloning into 'jadhcp'...
remote: Counting objects: 5, done.
jeff@juneau:~/docker$ which git
/usr/bin/git
jeff@juneau:~/docker$ git clone git@bitbucket.org:SeamanJeff/jadns.git
Cloning into 'jadns'...
remote: Counting objects: 10, done.
remote: Compressing objects: 100% (8/8), done.
remote: Total 10 (delta 2), reused 0 (delta 0)
Receiving objects: 100% (10/10), done.
Resolving deltas: 100% (2/2), done.
Checking connectivity... done.
~/docker$ 
```